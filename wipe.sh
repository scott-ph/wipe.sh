#!/bin/sh

str_repeat ()
{
	local str rep out
	str="$1"
	rep="$2"
	out=
	while [ "$rep" -gt 0 ]; do
	      out="$out$str"
	      rep=$(($rep - 1))
	done
	printf "%s" "$out"
}

substr ()
{
	local str start len drop_start keep drop
	str="$1"
	start="$2"
	len="$3"
	drop_start="$(str_repeat '?' "${start}")"
	str="${str#$drop_start}"
	keep="$(str_repeat '?' "${len}")"
	drop="${str#$keep}"
	str="${str%$drop}"
	printf "%s" "$str"
}

float_to_fixed ()
{
	local input neg int frac
	input=$1
	int=${input%.*}
	neg=
	if [ "$input" = "$int" ]; then
		input="$input."
	fi
	frac=1${input#${int}.}00000
	frac=$(($(substr "$frac" 0 5) - 10000))
	if [ "$int" -lt 0 ]; then
		int=$((-1 * $int))
		neg=-
	fi
	printf "%s%s" "$neg" "$(($int * 4096 + (($frac * 100000000) + 121070312) / 244140625))"
}

fixed_to_float ()
{
	local input int frac
	input=$1
	neg=
	if [ "$input" -lt 0 ]; then
		input=$((-1 * $input))
		neg=-
	fi
	int=$(($input / 4096))
	frac=$((($input - $int * 4096) * 244140625 / 10000000))
	printf "%s%s.%05d" "$neg" "$int" "$frac"
}

fixed_to_int ()
{

	echo $((($1+2048)/4096))
}

fixed_mul ()
{
	local a b sign
	sign=1
	if [ "$1" -lt 0 -a "$2" -ge 0 -o "$1" -ge 0 -a "$2" -lt 0 ]; then
		sign=-1
	fi
	a=$((($1 >> 12) * $2))
	b=$((((($1 & 0xfff) * $2) + $sign * 0x800) >> 12))
	printf "%s" "$(($a + $b))"
}

fixed_div ()
{
	local a b sign
	sign=1
	if [ "$1" -lt 0 -a "$2" -ge 0 -o "$1" -ge 0 -a "$2" -lt 0 ]; then
		sign=-1
	fi
	a=$((($1 / $2) << 12))
	b=$((((($1 - (($a * $2) >> 12)) << 12) + $sign * $2 / 2)/ $2))
	printf "%s" "$(($a + $b))"
}

add ()
{

	printf "%s" "$(fixed_to_float $(($(float_to_fixed $1) + $(float_to_fixed $2))))"
}

abs ()
{

	printf "%s" "${1#-}"
}

degrees_to_fixed ()
{
	local input int frac res neg
	input=$1
	int=${input%.*}
	if [ "$input" = "$int" ]; then
		input="$input."
	fi
	frac=${input#${int}.}00
	int=$((((${int}%360)+360)%360))
	if [ "$int" -ge 180 ]; then
		int=$((-1*(360-$int)))
	fi
	frac="$(substr "${frac}" 0 2)"
	res=$((${int} * 910222 + ${frac} * 9102))
	res=$((($res + ($res < 0 ? -1 : 1) * 5000) / 10000))
	printf "%s" "${res}"
}

sin ()
{
	local A1 B1 C1 a i n p q r y neg int frac

	i=$(degrees_to_fixed "$1")
	neg=
	if [ "$i" -lt 0 ]; then
		neg=-
	fi

	# Taken from:
	# https://www.nullhardware.com/blog/fixed-point-sine-and-cosine-for-embedded-systems/
	i=$(($i<<1))
	if [ "$i" -eq "$(($i | 0x4000))" ]; then
		i=$((0x8000 - $i))
	fi
	i=$((($i & 0x7fff) >> 1))

	A1=3370945099
	B1=2746362156
	C1=292421
	n=13
	p=32
	q=31
	r=3
	a=12

	y=$((($C1*($i))>>$n))
	y=$(($B1 - (($i*$y)>>$r)))
	y=$(($i * ($y>>$n)))
	y=$(($i * ($y>>$n)))
	y=$(($A1 - ($y>>($p-$q))))
	y=$(($i * ($y>>$n)))
	y=$((($y+(1<<($q-$a-1)))>>($q-$a)))

	printf "%s%s" "$neg" "$y"
}

cos ()
{
	sin "$(add "$1" 90)"
}

atan2 ()
{
	local y x coeff_1 coeff_2 abs_y r angle

	y=$1
	x=$2
	coeff_1=$(float_to_fixed 45)
	coeff_2=$(fixed_mul $coeff_1 $(float_to_fixed 3))
	abs_y=$(abs "$y")
	if [ "$abs_y" -eq 0 ]; then
		abs_y=1
	fi
	if [ "$x" -ge 0 ]; then
		r=$(fixed_div $(($x - $abs_y)) $(($x + $abs_y)))
		angle=$(($coeff_1 - $(fixed_mul $coeff_1 $r)))
	else
		r=$(fixed_div $(($x + $abs_y)) $(($abs_y - $x)))
		angle=$(($coeff_2 - $(fixed_mul $coeff_1 $r)))
	fi
	if [ "$y" -lt 0 ]; then
		angle=$((-1 * $angle))
	fi
	printf "%s" "$angle"
}

x ()
{
	printf "%s" ${1%,*}
}

y ()
{
	local y

	y=${1#*,}
	printf "%s" ${y%@*}
}

angle ()
{
	printf "%s" ${1#*@}
}

point ()
{
	local x y angle

	x=$1
	y=$2
	angle=$3

	if [ -n "$angle" ]; then
		printf "%s,%s@%s\n" "$x" "$y" "$angle"
	else
		printf "%s,%s\n" "$x" "$y"
	fi
}

line ()
{
	printf "%s-%s\n" $1 $2
}

start ()
{

	printf "%s" ${1%-*}
}

end ()
{

	printf "%s" ${1#*-}
}

reversed ()
{
	local i o

	o=
	for i; do
		o="$i${o:+ $o}"
	done
	printf "%s" "$o"
}

first ()
{
	printf "%s" "$1"
}

last ()
{
	printf "%s" $(first $(reversed "$@"))
}

draw ()
{
	local zig_zag rectangular brush_points b p o oc oco i

	zig_zag=$(get_zig_zag_path)
	rectangular=$(get_rectangular_path $(last $zig_zag))
	i=0

	for p in $zig_zag $rectangular; do
		b=$(get_brush_points $(x $p) $(y $p) $(angle $p))
		o="$o$(printf "%s" "$(draw_points '#' $b)")"
		oc="$oc$(printf "%s" "$(draw_points ' ' $b)")"
		if [ $(($i % 2)) -eq 0 ]; then
			printf "%s" "$o"
			printf "%s" "$oco"
			o=
			oco="$oc"
			oc=
		fi
		i=$(($i+1))
	done
	printf '\033[2J\033[H'
}

get_zig_zag_path ()
{
	local circle_points_left circle_points_right key_points turn_points step k p

	circle_points_left=$(reversed $(get_circle_points $HALF_BRUSH_WIDTH 90 270))
	circle_points_right=$(get_circle_points $HALF_BRUSH_WIDTH 270 450)
	key_points=$(get_key_points)
	step=0

	for k in $key_points; do
		get_line_points $(x $(start $k)) $(y $(start $k)) $(x $(end $k)) $(y $(end $k))
		if [ "$(($step % 2))" -eq 0 ]; then
			turn_points=$circle_points_right
		else
			turn_points=$circle_points_left
		fi
		for p in $turn_points; do
			point "$(($(x $p) + $(x $(end $k))))" "$(($(y $p) + $(y $(end $k)) + $HALF_BRUSH_WIDTH))" "$(angle $p)"
		done
		step=$((step+1))
	done
}

get_rectangular_path ()
{
	local closest_start_point vertical_margin horizontal_marge x y p

	closest_start_point=$1
	vertical_margin=$HALF_BRUSH_WIDTH
	horizontal_margin=$HALF_BRUSH_DEFORMED

	x=$(x $closest_start_point)
	while [ "$x" -gt -$FIXED_2 ]; do
		point "$x" "$(($FIXED_ROWS - $vertical_margin + $FIXED_1))" $FIXED_90
		x=$(($x - $FIXED_1))
	done

	for p in $(reversed $(get_circle_points $HALF_BRUSH_WIDTH 0 90)); do
		point "$(x $p)" "$(($(y $p) + $FIXED_ROWS - $(fixed_mul $vertical_margin $FIXED_2)))" "$(angle $p)"
	done

	y=$(($FIXED_ROWS - $vertical_margin - $FIXED_3))
	while [ "$y" -gt -$FIXED_1 ]; do
		point "$(($horizontal_margin - $FIXED_1))" "$y" $FIXED_180
		y=$(($y - $FIXED_1))
	done

	for p in $(reversed $(get_circle_points $HALF_BRUSH_WIDTH 90 180)); do
		point "$(($(x $p) + $(fixed_mul $horizontal_margin $FIXED_2)))" "$(y $p)" "$(angle $p)"
	done

	x=$(($horizontal_margin + $FIXED_3))
	while [ "$x" -lt $FIXED_COLUMNS ]; do
		point "$x" "$(($vertical_margin - $FIXED_1))" $FIXED_90
		x=$(($x + $FIXED_1))
	done

	for p in $(reversed $(get_circle_points $HALF_BRUSH_WIDTH 180 270)); do
		point "$(($(x $p) + $FIXED_COLUMNS))" "$(($(y $p) + $(fixed_mul $vertical_margin $FIXED_2)))" "$(angle $p)"
	done

	y=$(($vertical_margin + $FIXED_3))
	while [ "$y" -lt $FIXED_ROWS ]; do
		point "$(($FIXED_COLUMNS - $horizontal_margin))" "$y" $FIXED_180
		y=$(($y + $FIXED_1))
	done

	for p in $(reversed $(get_circle_points $HALF_BRUSH_WIDTH 270 360)); do
		point "$(($(x $p) + $FIXED_COLUMNS - $(fixed_mul $horizontal_margin $FIXED_2)))" "$(($(y $p) + $FIXED_ROWS))" "$(angle $p)"
	done

	x=$(($FIXED_COLUMNS - $horizontal_margin - $FIXED_3))
	while [ "$x" -gt $(x $closest_start_point) ]; do
		point "$x" "$(($FIXED_ROWS - $vertical_margin))" $FIXED_90
		x=$(($x - $FIXED_1))
	done
}

get_circle_points ()
{
	local radius start end angle angle_step
	radius=$1
	start=$2
	end=$3
	angle_step=5
	angle=$start
	while [ "$angle" -lt "$end" ]; do
		point "$(fixed_mul $(fixed_mul $(cos $angle) $radius) $DEFORMATION_FACTOR)" "$(fixed_mul $(sin $angle) $radius)" "$(float_to_fixed $angle)"
		angle=$(($angle + $angle_step))
	done
}

get_key_points ()
{
	local step a end

	step=$FIXED_0
	a=$(fixed_mul $(fixed_div $BRUSH_WIDTH $FIXED_2) $FIXED_3)
	while [ "$(($a + $(fixed_mul $(($step - $FIXED_1)) $BRUSH_WIDTH)))" -lt "$FIXED_ROWS" ]; do
		line \
		$(point                     "$(fixed_mul $HALF_BRUSH_DEFORMED $FIXED_2)"   "$(($(fixed_mul $HALF_BRUSH_WIDTH $FIXED_2) + $(fixed_mul $step $BRUSH_WIDTH)))") \
		$(point "$(($FIXED_COLUMNS - $(fixed_mul $HALF_BRUSH_DEFORMED $FIXED_2)))" "$(($(fixed_mul $HALF_BRUSH_WIDTH $FIXED_1) + $(fixed_mul $step $BRUSH_WIDTH)))")
		line \
		$(point "$(($FIXED_COLUMNS - $(fixed_mul $HALF_BRUSH_DEFORMED $FIXED_2)))" "$(($(fixed_mul $HALF_BRUSH_WIDTH $FIXED_3) + $(fixed_mul $step $BRUSH_WIDTH)))") \
		$(point                     "$(fixed_mul $HALF_BRUSH_DEFORMED $FIXED_2)"   "$(($(fixed_mul $HALF_BRUSH_WIDTH $FIXED_2) + $(fixed_mul $step $BRUSH_WIDTH)))")
		step=$(($step + $FIXED_1))
	done
}

get_line_points ()
{
	local start_x start_y end_x end_y y_step x_direction angle step end

	start_x=$1
	start_y=$2
	end_x=$3
	end_y=$4

	y_step=$(fixed_div $(($end_y - $start_y)) $(abs $(($end_x - $start_x))))
	x_direction=1
	if [ "$end_x" -lt "$start_x" ]; then
		x_direction=-1
	fi
	angle=$(($FIXED_90 + $(atan2 $(($end_y - $start_y)) $(($end_x - $start_x)))))
	step=0
	end=$(abs $(($end_x - $start_x)))
	while [ "$step" -lt "$end" ]; do
		point "$(($step * $x_direction + $start_x))" "$(($start_y + $(fixed_mul $y_step $step)))" "$angle"
		step=$((step + $FIXED_1))
	done
}

get_brush_points ()
{
	local x y angle new_x new_y opposite_angle step end

	x=$1
	y=$2
	angle=$3

	opposite_angle=$(($angle + $(float_to_fixed 180)))
	angle=$(fixed_to_float $angle)
	opposite_angle=$(fixed_to_float $opposite_angle)

	step=0
	end=$(fixed_to_int $(fixed_mul $HALF_BRUSH_WIDTH $DEFORMATION_FACTOR))
	while [ "$step" -lt "$end" ]; do
		new_x=$(($x + $(fixed_mul $(cos $angle) $(float_to_fixed $step))))
		new_y=$(($y + $(fixed_mul $(sin $angle) $(fixed_div $(float_to_fixed $step) $DEFORMATION_FACTOR))))
		point "$new_x" "$new_y"

		new_x=$(($x + $(fixed_mul $(cos $opposite_angle) $(float_to_fixed $step))))
		new_y=$(($y + $(fixed_mul $(sin $opposite_angle) $(fixed_div $(float_to_fixed $step) $DEFORMATION_FACTOR))))
		point "$new_x" "$new_y"

		step=$(($step + 1))
	done
}

draw_string_at ()
{
	local x y str
	x="$1"
	y="$2"
	str="$3"
	printf "\033[%d;%dH%s" $(($y + 1)) $(($x + 1)) "$str"
}

draw_points ()
{
	local character point x y

	character="$1"
	shift
	for point; do
		x=$(fixed_to_int $(x $point))
		y=$(fixed_to_int $(y $point))
		if [ "$x" -ge 0 -a "$y" -ge 0 -a "$x" -lt "$COLUMNS" -a "$y" -lt "$ROWS" ]; then
			draw_string_at "$x" "$y" "$character"
		fi
	done
}

FIXED_0=$(float_to_fixed 0)
FIXED_1=$(float_to_fixed 1)
FIXED_2=$(float_to_fixed 2)
FIXED_3=$(float_to_fixed 3)
FIXED_90=$(float_to_fixed 90)
FIXED_180=$(float_to_fixed 180)
COLUMNS=$(tput cols)
ROWS=$(tput lines)
FIXED_COLUMNS=$(float_to_fixed $COLUMNS)
FIXED_ROWS=$(float_to_fixed $ROWS)
BRUSH_WIDTH=$(float_to_fixed 6)
DEFORMATION_FACTOR=$FIXED_2
HALF_BRUSH_WIDTH=$(fixed_div $BRUSH_WIDTH $FIXED_2)
HALF_BRUSH_DEFORMED=$(fixed_div $(fixed_mul $BRUSH_WIDTH $DEFORMATION_FACTOR) $FIXED_2)
DELAY=3
FRAME_SPEED=7

printf '\033[?25l'
draw
printf '\033[?25h'
